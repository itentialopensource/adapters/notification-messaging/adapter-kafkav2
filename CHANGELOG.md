
## 0.23.1 [10-14-2024]

* Changes made at 2024.10.14_19:43PM

See merge request itentialopensource/adapters/adapter-kafkav2!33

---

## 0.23.0 [09-19-2024]

* switching dependencies

See merge request itentialopensource/adapters/adapter-kafkav2!31

---

## 0.22.7 [08-30-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-kafkav2!30

---

## 0.22.6 [08-14-2024]

* Changes made at 2024.08.14_17:49PM

See merge request itentialopensource/adapters/adapter-kafkav2!29

---

## 0.22.5 [08-07-2024]

* Changes made at 2024.08.06_18:45PM

See merge request itentialopensource/adapters/adapter-kafkav2!28

---

## 0.22.4 [07-29-2024]

* Patch/movemigration

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!27

---

## 0.22.3 [07-29-2024]

* Manual migration updates

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!26

---

## 0.22.2 [03-14-2024]

* Update metadata.json

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!24

---

## 0.22.1 [03-11-2024]

* Changes made at 2024.03.11_11:11AM

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!23

---

## 0.22.0 [01-22-2024]

* Minor/topic db dedupe

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!16

---

## 0.21.0 [01-22-2024]

* Minor/topic db dedupe

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!16

---

## 0.20.0 [01-22-2024]

* Minor/topic db dedupe

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!16

---

## 0.19.0 [01-08-2024]

* Migration changes

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!19

---

## 0.18.0 [01-08-2024]

* Migration changes

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!19

---

## 0.17.0 [01-08-2024]

* Migration changes

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!18

---

## 0.16.8 [12-10-2023]

* Fix bug to check allowed partition

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!17

---

## 0.16.7 [09-27-2023]

* Big fixes logLevel and adapter restart

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!15

---

## 0.16.6 [09-18-2023]

* Big fixes logLevel and adapter restart

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!15

---

## 0.16.5 [09-08-2023]

* Big fixes logLevel and adapter restart

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!15

---

## 0.16.4 [09-08-2023]

* Big fixes logLevel and adapter restart

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!15

---

## 0.16.3 [09-05-2023]

* Big fixes logLevel and adapter restart

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!15

---

## 0.16.2 [08-29-2023]

* Big fixes logLevel and adapter restart

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!15

---

## 0.16.1 [08-24-2023]

* Handle empty topics file

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!13

---

## 0.16.0 [08-23-2023]

* Add logic to clean topics.json from service config

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!11

---

## 0.15.0 [08-04-2023]

* Add logic to clean topics.json from service config

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!11

---

## 0.14.0 [08-04-2023]

* Add logic to clean topics.json from service config

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!11

---

## 0.13.0 [08-03-2023]

* Add seperate sasl props for consumer and producer

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!10

---

## 0.12.0 [08-03-2023]

* Add seperate sasl props for consumer and producer

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!10

---

## 0.11.0 [08-01-2023]

* Add seperate sasl props for consumer and producer

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!10

---

## 0.10.0 [07-19-2023]

* Add seperate sasl props for consumer and producer

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!10

---

## 0.9.0 [07-18-2023]

* Add seperate sasl props for consumer and producer

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!10

---

## 0.8.2 [07-18-2023]

* Update dependencies and add fromBeginnimg config

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!9

---

## 0.8.1 [07-10-2023]

* Revert "Merge branch 'patch/ADAPT-2647' into 'master'"

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!8

---

## 0.8.0 [05-25-2023]

* Add topic name to message

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!6

---

## 0.7.0 [05-19-2023]

* Add topic name to message

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!6

---

## 0.6.10 [05-19-2023]

* Bug fixes and performance improvements

See commit b0a14b5

---

## 0.6.9 [05-19-2023]

* added support for separate topics files per adapter

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!5

---

## 0.6.8 [05-19-2023]

* added support for separate topics files per adapter

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!5

---

## 0.6.7 [05-19-2023]

* added support for separate topics files per adapter

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!5

---

## 0.6.6 [05-19-2023]

* added support for separate topics files per adapter

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!5

---

## 0.6.5 [05-19-2023]

* added support for separate topics files per adapter

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!5

---

## 0.6.4 [05-19-2023]

* added support for separate topics files per adapter

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!5

---

## 0.6.3 [05-19-2023]

* added support for separate topics files per adapter

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!5

---

## 0.6.2 [05-16-2023]

* added support for separate topics files per adapter

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!5

---

## 0.6.1 [05-04-2023]

* added support for separate topics files per adapter

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!5

---

## 0.6.0 [04-10-2023]

* Turn Off stream if WFE or OpMgr are down

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!4

---

## 0.5.0 [03-13-2023]

* minor/adapt-2595

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!3

---

## 0.4.0 [02-28-2023]

* minor/adapt-2595

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!3

---

## 0.3.17 [02-20-2023]

* Updated readme

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!2

---

## 0.3.16 [02-20-2023]

* Updated readme

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!2

---

## 0.3.15 [02-17-2023]

* Updated readme

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!2

---

## 0.3.14 [02-16-2023]

* updated to allow multiple subscribers per topic entry and fixed partition logic

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!1

---

## 0.3.13 [02-07-2023]

* Bug fixes and performance improvements

See commit 311d4fb

---

## 0.3.12 [02-07-2023]

* Bug fixes and performance improvements

See commit 6c07dca

---

## 0.3.11 [09-29-2022]

* Added patch for SCRAM-SHA-256/512 support.

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!20

---

## 0.3.10 [09-29-2022]

* Fixed messages filtering. Allow regular expressions to be used as filters.

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!18

---

## 0.3.9 [08-22-2022]

* Fixed invalid input variable in `subscribe` task.

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!19

---

## 0.3.8 [08-05-2022]

* Prevent emitting duplicated messages to event subscribers.

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!17

---

## 0.3.7 [08-05-2022]

* Replaced 'writeTime' prop with 'interval_time' prop.

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!16

---

## 0.3.6 [08-05-2022]

* Fixed logging for consumed/dropped messages.

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!15

---

## 0.3.5 [08-05-2022]

* Improvments to handling 'offsetOutOfRange' errors.

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!14

---

## 0.3.4 [08-01-2022]

* Added kafka-node logger.

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!13

---

## 0.3.3 [08-01-2022]

* Added hostList property to adapter's config.

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!12

---

## 0.3.2 [07-29-2022]

* Offest changes

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!11

---

## 0.3.1 [01-18-2021]

* Patch/adapt 401

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!10

---

## 0.3.0 [12-01-2020]

* fix the partition and offset on the subscribe and subscribeAvro

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!9

---

## 0.2.2 [11-25-2020]

* Changes to have consuming Avro Messages work.

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!8

---

## 0.2.1 [07-23-2020]

* Patch/error handling

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!7

---

## 0.2.0 [07-15-2020]

* Minor/adapt 200

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!4

---

## 0.1.3 [04-21-2020]

* updates to the readme.md and properties

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!2

---

## 0.1.2 [04-20-2020]

* change return value on send and emmiter scope

See merge request itentialopensource/adapters/notification-messaging/adapter-kafka!1

---

## 0.1.1 [10-16-2019]

* Bug fixes and performance improvements

See commit 260fbf5

---
