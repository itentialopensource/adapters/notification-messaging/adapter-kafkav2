# Kafka v2

Vendor: Apache Kafka
Homepage: https://kafka.apache.org/

Product: Kafka 
Product Page: https://kafka.apache.org/

## Introduction

We classify Kafka v2 into the Notifications domain as Kafka v2 is used to send message between systems. There is an alternate Kafka adapter which uses an older Kafka library.

Apache Kafka is an open-source distributed event streaming platform used by thousands of companies for high-performance data pipelines, streaming analytics, data integration, and mission-critical applications.

"Deliver messages at network limited throughput using a cluster of machines with latencies as low as 2ms."
"Store streams of data safely in a distributed, durable, fault-tolerant cluster."

## Why Integrate
The Kafka v2 adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Kafka. With this adapter you have the ability to perform operations with Kafka on items such as:

- Produce/Publish messages onto message topics for others to consume
- Listen, receive and process messages placed on topics by other systems

## Additional Product Documentation
The [Kafka Documentation](https://kafka.apache.org/documentation/)
The [Kafkajs Node Library Documentation](https://www.npmjs.com/package/kafkajs)