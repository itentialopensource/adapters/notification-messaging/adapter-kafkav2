# Overview 
This adapter is used to integrate the Itential Automation Platform (IAP) with the Kafka System. The adapter utilizes the KafkaJS client to integrate with Apache Kafka. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.


## Details 
The Kafka v2 adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Kafka. With this adapter you have the ability to perform operations with Kafka on items such as:

- Produce/Publish messages onto message topics for others to consume
- Listen, receive and process messages placed on topics by other systems

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
