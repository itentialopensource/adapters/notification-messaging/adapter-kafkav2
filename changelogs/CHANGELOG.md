
## 0.16.8 [12-10-2023]

* Fix bug to check allowed partition

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!17

---

## 0.16.7 [09-27-2023]

* Big fixes logLevel and adapter restart

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!15

---

## 0.16.6 [09-18-2023]

* Big fixes logLevel and adapter restart

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!15

---

## 0.16.5 [09-08-2023]

* Big fixes logLevel and adapter restart

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!15

---

## 0.16.4 [09-08-2023]

* Big fixes logLevel and adapter restart

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!15

---

## 0.16.3 [09-05-2023]

* Big fixes logLevel and adapter restart

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!15

---

## 0.16.2 [08-29-2023]

* Big fixes logLevel and adapter restart

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!15

---

## 0.16.1 [08-24-2023]

* Handle empty topics file

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!13

---

## 0.16.0 [08-23-2023]

* Add logic to clean topics.json from service config

See merge request itentialopensource/adapters/notification-messaging/adapter-kafkav2!11

---