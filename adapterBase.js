/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global eventSystem log */
/* global cogs */
/* eslint no-underscore-dangle: warn  */
/* eslint no-loop-func: warn */
/* eslint no-cond-assign: warn */
/* eslint no-unused-vars: warn */
/* eslint consistent-return: warn */
/* eslint import/no-dynamic-require: warn */
/* eslint global-require: warn */
/* eslint no-await-in-loop: warn */

/* Required libraries.  */
const EventEmitterCl = require('events').EventEmitter;

class AdapterBase extends EventEmitterCl {
  constructor(prongid, properties) {
    super();

    this.props = properties;
    this.alive = false;
    this.healthy = false;
    this.id = prongid;
  }

  /**
   * getAllFunctions is used to get all of the exposed function in the adapter
   *
   * @function getAllFunctions
   */
  getAllFunctions() {
    let myfunctions = [];
    let obj = this;

    // find the functions in this class
    do {
      const l = Object.getOwnPropertyNames(obj)
        .concat(Object.getOwnPropertySymbols(obj).map((s) => s.toString()))
        .sort()
        .filter((p, i, arr) => typeof obj[p] === 'function' && p !== 'constructor' && (i === 0 || p !== arr[i - 1]) && myfunctions.indexOf(p) === -1);
      myfunctions = myfunctions.concat(l);
    }
    while (
      (obj = Object.getPrototypeOf(obj)) && Object.getPrototypeOf(obj)
    );
    return myfunctions;
  }

  /**
   * getWorkflowFunctions is used to get all of the workflow function in the adapter
   *
   * @function getWorkflowFunctions
   */
  getWorkflowFunctions(ignoreThes) {
    const myfunctions = this.getAllFunctions();
    const wffunctions = [];
    // remove the functions that should not be in a Workflow
    for (let m = 0; m < myfunctions.length; m += 1) {
      if (myfunctions[m] === 'checkIapAppsStatus') {
        // got to the second tier (adapterBase)
        break;
      }
      if (myfunctions[m] !== 'connect' && myfunctions[m] !== 'healthCheck'
        && myfunctions[m] !== 'getAllFunctions' && myfunctions[m] !== 'getWorkflowFunctions') {
        let found = false;
        if (ignoreThes && Array.isArray(ignoreThes)) {
          for (let i = 0; i < ignoreThes.length; i += 1) {
            if (myfunctions[m].toUpperCase() === ignoreThes[i].toUpperCase()) {
              found = true;
            }
          }
        }
        if (!found) {
          wffunctions.push(myfunctions[m]);
        }
      }
    }
    return wffunctions;
  }

  /**
   * @summary checkIapAppsStatus is used to check if any IAP apps are down.
   *
   * @function checkIapAppsStatus
   */
  async checkIapAppsStatus() {
    const origin = `${this.id}-adapterBase-checkIapAppsStatus`;
    log.trace(origin);

    let wfEngineActive = true;
    let opManagerActive = true;
    if (this.props && (this.props.check_wfe_status || this.props.check_wfe_status === undefined)
      && cogs.WorkFlowEngine && cogs.WorkFlowEngine.isActive) {
      wfEngineActive = await new Promise((resolve, reject) => {
        cogs.WorkFlowEngine.isActive((data, error) => {
          if (error || !data) {
            reject(error);
          } else {
            resolve(true);
          }
        });
      }).catch((error) => {
        log.error(error);
        return false;
      });
    }

    if (this.props && (this.props.check_ops_manager_status || this.props.check_ops_manager_status === undefined)
      && cogs.OperationsManager && cogs.OperationsManager.getTriggers) {
      opManagerActive = await new Promise((resolve, reject) => {
        cogs.OperationsManager.getTriggers({}, (data, error) => {
          if (error || !data) {
            reject(error);
          } else {
            resolve(true);
          }
        });
      }).catch((error) => {
        log.error(error);
        return false;
      });
    }
    return wfEngineActive && opManagerActive;
  }
}

module.exports = AdapterBase;
