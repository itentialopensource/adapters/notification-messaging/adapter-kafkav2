## Authenticating Kafka v2 Adapter 

This document will go through the steps for authenticating the Kafka v2 adapter with. Properly configuring the properties for an adapter in IAP is critical for getting the adapter online. You can read more about adapter authentication <a href="https://docs.itential.com/opensource/docs/authentication" target="_blank">HERE</a>. 

### Library Authentication
The Kafka v2 adapter supports SASL Authentication for Kafka v2 server. If you change authentication methods, you should change this section accordingly and merge it back into the adapter repository.

STEPS  
1. Ensure you have access to a Kafka v2 server and that it is running
2. Follow the steps in the README.md to import the adapter into IAP if you have not already done so
3. Use the properties below for the ```properties.client.sasl``` field
4. Kafka v2 adapter supports both PLAIN and SCRAM-SHA authentication mechanism

#### PLAIN
```json
"sasl": {
  "mechanism": "PLAIN",
  "username": "username",
  "password": "password"
}
```

#### SCRAM-SHA
```json
"sasl": {
  "mechanism": "scram-sha-512",
  "username": "username",
  "password": "password"
}
```
For more details on sasl authentication follow the README.md
4. Restart the adapter. If your properties were set correctly, the adapter should go online. 

### Troubleshooting
- Make sure you copied over the correct username and password.
- Turn on debug level logs for the adapter in IAP Admin Essentials.
- Investigate the logs
- Credentials should be ** masked ** by the adapter so make sure you verify the username and password - including that there are erroneous spaces at the front or end.